---
- name: Create Prometheus group
  ansible.builtin.group:
    name: "{{ prometheus['group'] }}"
    state: present

- name: Create Prometheus user
  ansible.builtin.user:
    name: "{{ prometheus['user'] }}"
    group: "{{ prometheus['group'] }}"
    shell: /usr/bin/false
    state: present

- name: Download Prometheus
  ansible.builtin.unarchive:
    src: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus['version'] }}/prometheus-{{ prometheus['version'] }}.linux-amd64.tar.gz
    remote_src: yes
    dest: /tmp

- name: Create Prometheus directories
  ansible.builtin.file:
    path: "{{ item }}"
    owner: "{{ prometheus['user'] }}"
    group: "{{ prometheus['group'] }}"
    state: directory
  with_items:
    - /etc/prometheus
    - /var/lib/prometheus

- name: Copy Prometheus Binaries
  ansible.builtin.copy:
    src: "/tmp/prometheus-{{ prometheus['version'] }}.linux-amd64/{{ item }}"
    dest: "/usr/local/bin/{{ item }}"
    remote_src: yes
  with_items:
    - prometheus
    - promtool

- name: Fix binaries ownership
  ansible.builtin.file:
    path: "/usr/local/bin/{{ item }}"
    owner: "{{ prometheus['user'] }}"
    group: "{{ prometheus['group'] }}"
    mode: "0775"
    state: file
  with_items:
    - prometheus
    - promtool

- name: Copy ETC items
  ansible.builtin.copy:
    src: "/tmp/prometheus-{{ prometheus['version'] }}.linux-amd64/{{ item }}"
    dest: /etc/prometheus
    remote_src: yes
    directory_mode: yes
  with_items:
    - consoles
    - console_libraries

- name: Fix ETC ownership
  ansible.builtin.file:
    path:  /etc/prometheus
    owner: "{{ prometheus['user'] }}"
    group: "{{ prometheus['group'] }}"
    recurse: yes
    state: directory

- name: Copy Prometheus Config File
  ansible.builtin.copy:
    src: etc-prometheus.yml
    dest: /etc/prometheus/prometheus.yml

- name: Include Firewall files when required
  block:
    - name: Install Python Modules
      ansible.builtin.shell:
        cmd: python3 -m pip install "{{ item }}"
      loop: "{{ pip_modules }}"

    - name: Include firewall tasks
      ansible.builtin.include_tasks:
        file: "{{ ansible_facts['os_family']|lower }}-firewall.yml"
  when:
    - prometheus['firewalled'] == true
    - ansible_facts['distribution']|lower != 'amazon'

- name: Copy Prometheus Service Unit
  ansible.builtin.copy:
    src: prometheus.service
    dest: /etc/systemd/system/prometheus.service

- name: Reload systemd daemon
  ansible.builtin.systemd:
    name: prometheus.service
    daemon_reload: yes
    state: started
  tags:
    - notest
    - molecule-notest

